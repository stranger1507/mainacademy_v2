FROM mediasapiens/python3

WORKDIR /web
COPY requirements.txt /requirements.txt

RUN pip install -r /requirements.txt
