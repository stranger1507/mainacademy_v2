# l1 = [1, 2, 4, 5]
# l2 = l1
# print(id(l1) == id(l2))
# print(l2, l1)
# l1.extend([3, 33, 333])
# print(l2, l1)
# from copy import copy, deepcopy
#
# a = [1, 2, 3]
# b = [4, 5, 6]
# c = [a, b]
# d = c
#
# print(id(c) == id(d))
# print(id(c[0]) == id(d[0]))
#
# d = copy(c)
#
# print(id(c) == id(d))
# print(id(c[0]) == id(d[0]))
#
# d = deepcopy(c)
#
# print(id(c) == id(d))
# print(id(c[0]) == id(d[0]))
# a = 'qwe'
# b = 'hello world'
# c = 'sdssss'
# print("{0} {1} {0} \t\n {2}".format(a, b, c))
# print("%s %s" % (a, b))
#
# a = input('Введите слово:\n')
# print(a, type(a))
# try:
#     raise KeyError('KeyError')
#     raise ValueError('ValueError')
#     print('hi')
# except Exception as e:
#     print(e)
# i = 0
# while i != 15:
#     print(i)
#     i += 1
#
# for x in [1, 2, 3, 4]:
#     print(x)
# for x in 'qwas':
#     print(x*3)
# a = ''
# while len(a) < 3:
#     a = input('Введите a:\n')
# for x in 'hello world':
#     if x == 'l':
#         continue
#     print(x)
# a = 0
# while a < 10:
#     a += 1
#     if a >= 2 and a <= 4:
#         continue
#     print(a)
# for x in 'aaaoaaa':
#     if x == 'o':
#         break
#     print(x)
# i = 0
# while i < 999999999999999999:
#     i += 1
#     if i > 10:
#         break
#     print(i)
# for x in 'hello world':
#     print(x)
# else:
#     print('Завезите больше букв')
# for x in 'hello world':
#     print(x)
#     break
# else:
#     print('Завезите больше букв')
# def add(x, y):
#     return x + y
#
# print(add(1, 2))
# def newfunc(n):
#     def my(x):
#         return x + n
#     return my
#
# a = newfunc(100)
# print(a(1))
#
# def odd(a, b):
#     print('+', add(a, b))
#     return a - b
#
#
# def add(a, b):
#     return a + b
#
#
# def calc(a, b):
#     return odd(a, b)
# print(calc(1, 2))

# def new_print(a, c='python3'):
#     print(a, c)
#
# new_print('I like')
# new_print(c='football', a='I hate')
#
# def arg(*args):
#     print(args)
#
# arg(1,2,3,'qwe')
#
# def arg(a, b, *args):
#     print(a, b)
#     print(args)
#
#
# arg(1, 2, 3, 'qwe', 23423, 234, 23)
#
# def func(**kwargs):
#     print(kwargs)
#
# func(a=1, v=2, c=4)

# def func(*args, **kwargs):
#     print(kwargs)
#     print(args)
#
#
# func(1, 4, 5, a=1, v=2, c=4, )

#
# def func(**kwargs):
#     print(kwargs)

# d = {'a': 1, 'b': 2}
# func(**d)

#
# def func(*args, **kwargs):
#     print(kwargs)
#
#
# func(1, 2, 3, 5, a=2, b=7)

# func = lambda x, y: x + y
#
# print(func(1,2))

# print((lambda x, y: x + y)(1, 2))
# func = lambda *args, **kwargs: print(args, '\t', kwargs)
# func(1, 4, 5, a=1, v=2, c=4, )

class Calculator:
    a = ''
    b = ''

    def __init__(self, a, b):
        print('-' * 50)
        print('+', self.a, '+', self.b, '+')
        self.a = str(a)
        self.b = str(b)
        print(self.a, self.b)
        print('-' * 50)

    def get_a(self, ):
        self.a = input('Введите а:')

    def get_b(self, ):
        self.b = input('Введите b:')

    def cal(self, d):
        print(eval("".join([self.a, d, self.b])))


calc = Calculator(1, 2)
calc.cal('+')
calc.cal('-')
