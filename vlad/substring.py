def z_function(s):
    n = len(s)
    z = [0] * n
    l = 0
    r = 0
    for i in range(1, n):
        if i <= r:
            z[i] = min(r - i + 1, z[i - 1])
        while i + z[i] < n and s[z[i]] == s[i + z[i]]:
            z[i] += 1
        if i + z[i] - 1 > r:
            l = i
            r = i + z[i] - 1
    return z


s = "wowhatamanwowowpalehche"
subs = "wow"

s = subs + "#" + s
summary = 0

array = z_function(s)

for i in range(0, len(s)):
    if array[i] == len(subs):
        summary += 1

print (summary)
