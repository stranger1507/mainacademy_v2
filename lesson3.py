# class SimpleIterator:
#     def __init__(self, fname):
#         self.fd = open(fname, 'r')
#
#     def __iter__(self):
#         return self
#
#     def __next__(self):
#         l = self.fd.readline()
#         if l != '':
#             l = l.rstrip('\n')
#             num = int(l)
#             return num * 2
#         raise StopIteration
#
#
# a = SimpleIterator('d.txt')
# for x in a:
#     print(x)
# import os
# print(os.getcwd())
# from os import getcwd
# print(getcwd())
# import time, random, os
# from random import randint, random
# import os as operationSystem
# print(operationSystem.getcwd())

# from os import getcwd as cwd, scandir as sc
# from os import *

# from tatyana.Degrees import degr
# degr(45, 'c')


class A:
    a = 200

    def _maybe_private(self):
        print('May be private')

    def __private(self):
        print('I am super private method')

    def func(self):
        self._maybe_private()
        self.__private()


# object_a = A()
# object_a._maybe_private()
# object_a.__private()
# object_a._A__private()
#
# class B(A):
#     a = 2
#     def func(self):
#         print('qwe')
#
# object_b = B()
# object_b._maybe_private()
# print(object_b.a)
# object_b.func()
#
# print(1+1)
# print('1'+'1')

# class A:
#     def go(self):
#         print('A')
#
#
# class B():
#     def go(self):
#         print('B')
#
#
# class C(A, B):
#     def go(self):
#         print('C')
#
# c = C()
# c.go()
# class A:
#     def go(self):
#         print('A')
#
#
# class B():
#     def go(self):
#         print('B')
#
#
# class C(A, B):
#     pass
#
# c = C()
# c.go()
# class A:
#     def go(self):
#         print('A')
#
#
# class B:
#     def go(self):
#         print('B')
#
#
# class C(A, B,):
#     def go(self):
#         # print('C')
#         super(C, self).go()
#
#
# class E(A):
#     def go(self):
#         print('E')
#         super(E, self).go()
#
#
# e = E()
# e.go()

