# Задание 5: определение типа.
# УСЛОВИЕ:
# функция, которая принимает объект и выводит строку с наименованием типа этого объекта.
# Пример:
# typer(666) == "int"
# typer("666") == "str"
# typer(typer) == "function"

def typers(s):
    if type(s) is int:
        print("typer(" + str(s) + ") == 'int'")
    if type(s) is str:
        print("typer(" + str(s) + ") == 'str'")
    if type(s) is function:
            print("typer(" + str(s) + ") == 'function'")



