# Задание 3: подстчет вхождений подстроки.
# УСЛОВИЕ:
# Реализовать подсчеы количества вхождений подстроки "wow" в строке.
# ВХОД: строка
# ВЫХОД: число вхождений подстроки "wow"
# Пример:
# s = "wowhatamanwowowpalehche"
# ...
# = 3

# s = "wowhatamanwowowpalehche"
# s_lower = s.lower()
# cnt_s_lower = s_lower.count('wow')
# print(cnt_s_lower)
s = "wowhatamanwowowpalehche"
n = 0
result = 0
while n < len(s):
    if s[n:n + 3] == 'wow':
        result += 1
    n += 1
print(result)

