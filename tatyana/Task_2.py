# Задание 2: подсчет гласных.
# УСЛОВИЕ:
# подсчет гласных букв в строке.
# Примечание:
# - для простоты на вход принимаем строку из букв латинского алфавита;
# - набор гласных принимаем за 'a', 'e', 'i', 'o', 'u', 'y';
# - программа должна быть нечувствительна к регистру.
# Пример:
# s = "hApPyHalLOweEn!"
# ...
# print result
# > 5

#1 способ:
# s = input("Введите любую строку символов:")
# new_s = s.lower()
# a_count = new_s.count('a')  # не оптимальный вариант
# e_count = new_s.count('e')
# i_count = new_s.count('i')
# o_count = new_s.count('0')
# u_count = new_s.count('u')
# result = int(a_count) + int(e_count) + int(i_count) + int(o_count) + int(u_count)
# print(result)

# 2 способ:
# s_2 = input("Введите любую строку символов:")
# new_s_2 = s_2.lower()
# l = list(new_s_2)
# letters = ['a', 'e', 'i', 'o', 'u']
# cnt = 0
# for i in l:
#     if i in letters:
#         cnt = cnt + 1
#     else:
#         continue

# print(cnt)
s = "hApPyHalLOweEn!".lower()
sum = 0
letters = ['a', 'e', 'i', 'o', 'u']
for letter in letters:
    sum += s.count(letter)
print(sum)
