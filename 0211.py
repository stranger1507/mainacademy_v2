try:
    print('try')
    raise ValueError
except Exception:
    print('except')
else:
    print('else')
finally:
    print('finally')

for x in range(1, 11):
    print(x)
    break
else:
    print('finish')
