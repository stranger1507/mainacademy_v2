# Реализовать класс Person, который отображает запись в книге контактов.
#
# Класс имеет 4 атрибута:
#  - surname - строка - фамилия контакта (обязательный)
# - first_name - строка - имя контакта (обязательный)
# - nickname - строка - псевдоним (опциональный)
# - birth_date - объект datetime.date (обязательный)
#
# Каждый вызов класса должен создавать экземпляр (инстанс) класса с указанными атрибутами.
#
# Также класс имеет 2 метода:
# - get_age() - считает возраст контакта в полных годах на дату вызова и
# возвращает строку вида: "27";
# - get_fullname() - возвращает строку, отражающую полное имя (фамилия + имя) контакта;
from datetime import datetime

from dateutil.parser import parse


class Person:

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key == 'birth_date' and not value:
                value = '30-10-2014'
            setattr(self, key, value)
        # self.nickname = kwargs.get('nickname')
        # self.first_name = kwargs.get('first_name')
        # self.surname = kwargs.get('last_name')
        # self.birth_date = kwargs.get('birth_date') or '30-10-2014'

    def get_fullname(self):
        print("{1}, {0}.".format(self.last_name, self.first_name))

    def get_age(self):
        try:
            now = datetime.now().year

            age = now - parse(self.birth_date).year
            print("Возраст {1} {0} = '{2}'".format(self.last_name, self.first_name, age))
        except:
            print('Прости не смог посчитпть')


text_data = {
    'nickname': "Никнейм:", 'first_name': 'Имя: ', 'last_name': 'Фамилия: ',
    'birth_date': 'Дата рождения(30-10-2014): ', 'new_data': 'New data:'
}
data = {}
for attr, text in text_data.items():
    data[attr] = input(text)

people = Person(**data)
people.get_fullname()
people.get_age()

