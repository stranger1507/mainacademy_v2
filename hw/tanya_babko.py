# Реализовать класс Person, который отображает запись в книге контактов.
#
# Класс имеет 4 атрибута:
#  - surname - строка - фамилия контакта (обязательный)
# - first_name - строка - имя контакта (обязательный)
# - nickname - строка - псевдоним (опциональный)
# - birth_date - объект datetime.date (обязательный)
#
# Каждый вызов класса должен создавать экземпляр (инстанс) класса с указанными атрибутами.
#
# Также класс имеет 2 метода:
# - get_age() - считает возраст контакта в полных годах на дату вызова и
# возвращает строку вида: "27";
# - get_fullname() - возвращает строку, отражающую полное имя (фамилия + имя) контакта;
from datetime import datetime


class Person:

    def __init__(self, nickname, first_name, surname, birth_date):
        self.nickname = nickname
        self.first_name = first_name
        self.surname = surname
        self.birth_date = birth_date

    def nickname(self):
        return self._nickname

    def nickname(self, nickname):
        if type(nickname) is str:
            self.__nickname = nickname
        else:
            print("Недопустимый формат")

    def first_name(self):
        return self._first_name

    def first_name(self, first_name):
        if type(first_name) is str:
            self.__first_name = first_name
        else:
            print("Недопустимый формат")

    def surname(self):
        return self._surname

    def surname(self, surname):
        if type(surname) is str:
            self.__surname = surname
        else:
            print("Недопустимый формат")

    def birth_date(self):
        return self._birth_date

    def birth_date(self, birth_date):
        if type(birth_date) is str:
            self.__birth_date = birth_date
        else:
            print("Недопустимый формат")

    def get_fullname(self):
        print("{1}, {0}.".format(self.surname, self.first_name))

    def get_age(self):
        now = datetime.now()
        birth_date_person = self.birth_date.split('-')
        date_birth_date_person = datetime(int(birth_date_person[0]), int(birth_date_person[1]),
                                          int(birth_date_person[2]))
        period_days = now - date_birth_date_person
        period = int(int(period_days.days) / 365.25)
        print("Возраст {1} {0} = '{2}'".format(self.surname, self.first_name, period))


print("Для оперделения человека задайте основные параметры:")
name_person = input("Имя:")
sername_person = input("Фамилия:")
nickname_person = input("Никнейм:")
day_date_person = input("День рождения:")
month_date_person = input("Месяц рождения:")
year_date_person = input("Год рождения:")
birth_date = year_date_person + "-" + month_date_person + "-" + day_date_person
people = Person(nickname_person, name_person, sername_person, birth_date)
people.get_fullname()
people.get_age()
