leng12 = {"1": "3", "10": "3",
          "2": "3", "11": "6",
          "3": "5", "12": "5",
          "4": "4", "0": "0",
          "5": "4",
          "6": "3",
          "7": "5",
          "8": "5",
          "9": "4"}

leng = {"2": "4",
        "3": "4", "0": "0",
        "4": "4",
        "5": "3",
        "6": "3",
        "7": "5",
        "8": "4",
        "9": "4", }


def getlen12(num):
    num = str(num)
    getlen = int(leng12.get(num))
    return getlen


def getlen13_19(num):
    num = str(num)
    getlen = int(leng.get(num[1])) + int(4)
    return getlen


def getlen20_99(num):
    num = str(num)
    getlen = int(leng.get(num[0])) + 2 + getlen12(num[1])
    return getlen


def getlen99(num):
    if num < 13:
        getlen = getlen12(num)
    elif num >= 13 and num < 20:
        getlen = getlen13_19(num)
    else:
        getlen = getlen20_99(num)
    return getlen


def getlen_hun(num):
    hun = num // 100
    ten = num % 100
    hun = str(hun)
    if ten != 0:
        getlen = int(getlen12(hun)) + int(10) + getlen99(ten)
    elif ten == 0:
        getlen = int(getlen12(hun)) + int(7)
    return getlen


def main():
    num = 343
    total = 0

    print(num)
    if num <= 99:
        total += getlen99(num)
    elif num >= 100 and num < 1000:
        total += getlen_hun(num)
    elif num == 1000:
        total += int(11)
    else:
        print('something wrong')
    num = int(num) + 1

    print(total, "three hundred forty three", len("three hundred forty three"))


main()
