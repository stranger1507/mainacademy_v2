# a = 4
# c = 6
# action = '^'
#
# d = {
#     '+': a + c,
#     '-': a - c,
#     '*': a * c,
#     '/': a / c,
# }
# if action == '+':
#     print(a+c)
# elif action == '-':
#     print(a - c)
# elif action == '*':
#     print(a * c)
# elif action == '/':
#     print(a / c)
# else:
#     print("I don't have the result")
# print(d.get('ww'))
# try:
#     calc = d['&']
# except KeyError:
#     calc = 'qwefffff'
#
# print(calc)
# a = str()
# print(type(a), type('fff'))
# print(type(int()))
# float()
# dict()
# list()
# tuple()
# set()
# bool()
# a = 4
# print(type(4), type(str(4)))
#
# a = True
# a = False
# a = None
# print(a is None, bool(a))

# str = 'qqwe'
# print(str, type(str))

# a = 'mama'
# print(len(a))

# string
# a = 'I like python'
# print(a.title())
# print(a.capitalize())
# print(a.upper())
# print(a.lower())
# print(a + a)

# list
l = ['qwe', 'ee', 'dc']
a = [5, 1, 2, 3, 4, 5, 6]
# print(str(l))
# print(l[0] + ' ' + l[1] + ' ' + l[2] + ' ' + a + str(432))
# print("+".join(l))
# print(l + l)
# print(l.append('ads'), l)
# print(l.extend(a), l)
# print(l.insert(0, '11'), l)
# l[0] = '222'
# print(l)
# print('qw', 'ee', 'e')
# print(l.remove('ee'), l)
# del l[2]
# print(l)
# print(a[2:4])
# print(a[2:])
# print(a[:2])
# print(a[:])
# print(a[::2])
# print(a[::2])
# print('i like python3'[::-1])
# print(a[3::2])
# print(a * a)
# print(l.clear(), l)
# print(a.sort(), a)
# print(a.reverse(), a)
# a.sort()
# print(a.reverse(), a)
# print(a.reverse(), a)
# print(a.pop(), a)
# print(a.count(5), a)
# print(len(a), len('qwe'))
# print(l.pop(), l)
# print(l.pop(1), l)
d = {'a': 1, 1: 3, 5: 55}
# print(d.keys())
# print(d.values())
# print(d.clear(), d)
# print(d.items())
# print(d.pop(5), d)
# print(d.setdefault('vagner', 'aaaa'), d)
# print(d.setdefault('vagner'), d)
# print(l.extend([11,2,333,4,5]), l)
# print(d.update({'masha': 'cool'}), d)
# print(d.update({'a': 2, 1: 444, 5: 525}), d)
# print(d.get('qwe'))
# print(d.get('qwe', 'asd'), d)
# t = (1, 3,)
# print(type(t), type((1)), type((1,)))
# print(type(t), type(('123')), type(('123',)))
#
